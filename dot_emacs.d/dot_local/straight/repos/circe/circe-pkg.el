(define-package "circe" "2.12" "Client for IRC in Emacs"
  '((emacs "24.5") (cl-lib "0.5"))
  :url "https://github.com/emacs-circe/circe")
