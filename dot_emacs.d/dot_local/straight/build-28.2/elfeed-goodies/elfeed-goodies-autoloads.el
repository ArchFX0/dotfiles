;;; elfeed-goodies-autoloads.el --- automatically extracted autoloads  -*- lexical-binding: t -*-
;;
;;; Code:


;;;### (autoloads nil "elfeed-goodies" "elfeed-goodies.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from elfeed-goodies.el

(autoload 'elfeed-goodies/setup "elfeed-goodies" "\
Setup Elfeed with extras:

* Adaptive header bar and entries.
* Header bar using powerline.
* Split pane view via popwin." t nil)

;;;***

;;;### (autoloads nil "elfeed-goodies-logging" "elfeed-goodies-logging.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from elfeed-goodies-logging.el

(autoload 'elfeed-goodies/toggle-logs "elfeed-goodies-logging" "\
Toggle the display of Elfeed logs in a popup window." t nil)

;;;***

;;;### (autoloads nil nil ("elfeed-goodies-new-entry-hooks.el" "elfeed-goodies-search-mode.el"
;;;;;;  "elfeed-goodies-show-mode.el" "elfeed-goodies-split-pane.el")
;;;;;;  (0 0 0 0))

;;;***

(provide 'elfeed-goodies-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; elfeed-goodies-autoloads.el ends here
