;;; ccc-autoloads.el --- automatically extracted autoloads  -*- lexical-binding: t -*-
;;
;;; Code:


;;;### (autoloads nil "ccc" "ccc.el" (0 0 0 0))
;;; Generated autoloads from ccc.el

(autoload 'ccc-setup "ccc" nil nil nil)

(autoload 'ccc-update-buffer-local-frame-params "ccc" "\


\(fn &optional BUFFER)" nil nil)

;;;***

(provide 'ccc-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; ccc-autoloads.el ends here
