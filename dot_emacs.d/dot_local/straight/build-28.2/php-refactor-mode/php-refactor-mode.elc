;ELC   
;;; Compiled
;;; in Emacs version 28.2
;;; with all optimizations.



(byte-code "\300\301!\210\300\302!\210\303\304\305\306\307\310\307\311&\210\312\313\314\315\307\304\316\317&\210\312\320\321\322\307\304\316\317&\210\312\323\324\325\307\304\316\326&\207" [require thingatpt locate custom-declare-group php-refactor nil "Quickly and safely perform common refactorings." :group tools convenience custom-declare-variable php-refactor-command "refactor.phar" "Define the command used for executing the refactoring." :type symbol php-refactor-patch-command "patch -p1 --no-backup-if-mismatch" "Define the command used for applying the patch." php-refactor-keymap-prefix (kbd "C-c r") "The php-refactor keymap prefix." string] 8)
#@31 Keymap for php-refactor mode.
(defvar php-refactor-mode-map (byte-code "\303 \303 \304	\305\306#\210\304	\307\310#\210\304	\311\312#\210\304	\313\314#\210\304\n	#\210))\207" [map prefix-map php-refactor-keymap-prefix make-sparse-keymap define-key "lv" php-refactor--convert-local-to-instance-variable "rv" php-refactor--rename-local-variable "em" php-refactor--extract-method "ou" php-refactor--optimize-use] 4) (#$ . 745))
#@103 Non-nil if Php-Refactor mode is enabled.
Use the command `php-refactor-mode' to change this variable.
(defvar php-refactor-mode nil (#$ . 1180))
(make-variable-buffer-local 'php-refactor-mode)
#@602 Minor mode to quickly and safely perform common refactorings.

This is a minor mode.  If called interactively, toggle the
`Php-Refactor mode' mode.  If the prefix argument is positive, enable
the mode, and if it is zero or negative, disable the mode.

If called from Lisp, toggle the mode if ARG is `toggle'.  Enable the
mode if ARG is nil, omitted, or is a positive number.  Disable the
mode if ARG is a negative number.

To check whether the minor mode is enabled in the current buffer,
evaluate `php-refactor-mode'.

The mode's hook is called both when the mode is enabled and when it is
disabled.
(defalias 'php-refactor-mode #[(&optional arg) "\305 	\306=\203 \n?\202 	\247\203 	\307W\203 \310\202 \311\312\303!\2032 \313\302\"\n\2032 \302B\314\315\n\203< \316\202= \317\"\210\320\321!\203b \305 \203Q \305 \232\203b \322\323\324\n\203] \325\202^ \326\f#\210))\327 \210\n\207" [#1=#:last-message arg php-refactor-mode local-minor-modes local current-message toggle 1 nil t boundp delq run-hooks php-refactor-mode-hook php-refactor-mode-on-hook php-refactor-mode-off-hook called-interactively-p any " in current buffer" message "Php-Refactor mode %sabled%s" "en" "dis" force-mode-line-update] 4 (#$ . 1380) (list (if current-prefix-arg (prefix-numeric-value current-prefix-arg) 'toggle))])
(defvar php-refactor-mode-hook nil)
(byte-code "\301\302N\204\f \303\301\302\304#\210\303\301\305\306#\210\303\301\307\310C#\210\311\312\313\310\211%\207" [php-refactor-mode-map php-refactor-mode-hook variable-documentation put "Hook run after entering or leaving `php-refactor-mode'.\nNo problems result if this variable is not bound.\n`add-hook' automatically binds it.  (This is true for all hook variables.)" custom-type hook standard-value nil add-minor-mode php-refactor-mode " Refactor"] 6)
#@66 Convert a local variable into an instance variable of the class.
(defalias 'php-refactor--convert-local-to-instance-variable #[nil "\300\301\302 \303 \304\305!$\207" [php-refactor--run-command "convert-local-to-instance-variable" buffer-file-name php-refactor--get-effective-line-number-as-string thing-at-point sexp] 6 (#$ . 3192) nil])
#@55 Optimizes the use of Fully qualified names in a file.
(defalias 'php-refactor--optimize-use #[nil "\300\301\302 \"\207" [php-refactor--run-command "optimize-use" buffer-file-name] 3 (#$ . 3536) nil])
#@160 Extract the selected region into a separate method.

BEGIN is the starting position of the selected region.
END is the ending position of the selected region.
(defalias 'php-refactor--extract-method #[(begin end) "\305\306!!\305\306	!!\307\310!\311\312\313 \f\314Q\n$+\207" [begin end method region-end region-start number-to-string line-number-at-pos read-from-minibuffer "Specify new method name: " php-refactor--run-command "extract-method" buffer-file-name "-"] 6 (#$ . 3743) "r"])
#@62 Rename an existing local variable to the specified new name.
(defalias 'php-refactor--rename-local-variable #[nil "\301\302!\303\304\305 \306 \307\310!%)\207" [renamed read-from-minibuffer "Specify new variable name: " php-refactor--run-command "rename-local-variable" buffer-file-name php-refactor--get-effective-line-number-as-string thing-at-point sexp] 6 (#$ . 4240) nil])
#@73 Retrieve the current line number as a string, accounting for narrowing.
(defalias 'php-refactor--get-effective-line-number-as-string #[nil "\214~\210\300\301 !)\207" [number-to-string locate-current-line-number] 2 (#$ . 4625)])
#@154 Execute the given refactoring command and apply the resulting patch.

ARGS contains a list of all the arguments required for the specific method to run.
(defalias 'php-refactor--run-command #[(&rest args) "\304 \210\305`\nB\306\307!!\210\310\311\312\211#\210b*\207" [temp-point revert-buffer-function buffer-undo-list args save-buffer php-refactor--revert-buffer-keep-history shell-command php-refactor--generate-command revert-buffer nil t] 4 (#$ . 4860)])
#@138 Build the appropriate command to perform the refactoring.

ARGS contains a list of all the arguments required to generate a refactoring.
(defalias 'php-refactor--generate-command #[(args) "\301\302!!\207" [args php-refactor--append-patch-command php-refactor--generate-refactor-command] 3 (#$ . 5331)])
#@150 Build the portion of the command required to perform the refactoring.

ARGS contains a list of all the arguments required to generate a refactoring.
(defalias 'php-refactor--generate-refactor-command #[(args) "	B\304\n!\305\306\307#*\207" [php-refactor-command args refactor-command-list refactor-command-args php-refactor--quote-arg-list mapconcat identity " "] 4 (#$ . 5642)])
#@108 Run all arguments through 'shell-quote-argument'.

ARGS a list of individual command arguments to protect.
(defalias 'php-refactor--quote-arg-list #[(args) "\301\302\"\207" [args mapcar shell-quote-argument] 3 (#$ . 6032)])
#@132 Build the command to pipe the refactor command into patch.

REFACTOR-COMMAND The 'shell-command' portion to execute a refactoring.
(defalias 'php-refactor--append-patch-command #[(refactor-command) "\302	Q\207" [refactor-command php-refactor-patch-command " | "] 3 (#$ . 6264)])
#@60 Revert the buffer contents while perserving the undo tree.
(defalias 'php-refactor--revert-buffer-keep-history #[(&optional _IGNORE-AUTO _NOCONFIRM _PRESERVE-MODES) "\300 \210~\210ed|\210\301\302 !\210\303 \210\304 \207" [clear-visited-file-modtime insert-file-contents buffer-file-name not-modified set-visited-file-modtime] 2 (#$ . 6550)])
(provide 'php-refactor-mode)
