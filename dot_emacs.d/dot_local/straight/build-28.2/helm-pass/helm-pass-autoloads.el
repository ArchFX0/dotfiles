;;; helm-pass-autoloads.el --- automatically extracted autoloads  -*- lexical-binding: t -*-
;;
;;; Code:


;;;### (autoloads nil "helm-pass" "helm-pass.el" (0 0 0 0))
;;; Generated autoloads from helm-pass.el

(autoload 'helm-pass "helm-pass" "\
Helm interface for pass." t nil)

;;;***

(provide 'helm-pass-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; helm-pass-autoloads.el ends here
