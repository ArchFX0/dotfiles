#!/bin/sh

xrdb merge ~/.Xresources 
xbacklight -set 40 &
nitrogen --restore
# xset r rate 200 50 &
picom &
dunst &

exec dwm
